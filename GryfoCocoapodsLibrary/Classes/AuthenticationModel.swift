public protocol AuthenticationModel: Model {}

public struct LocalAuthenticationModel: AuthenticationModel {
    public var token: String?
    
    public init(token: String? = nil) {
        self.token = token
    }
}

public struct RemoteAuthentucationModel: AuthenticationModel {
    public var accesLevel: String?
    public var businessModel: String?
    public var companyId: String?
    public var isActive: Bool?
    public var username: String?
    
    public init(accesLevel: String? = nil, businessModel: String? = nil, companyId: String? = nil, isActive: Bool? = nil, username: String? = nil) {
        self.accesLevel = accesLevel
        self.businessModel = businessModel
        self.companyId = companyId
        self.isActive = isActive
        self.username = username
    }
}
