import Foundation

public class GetDeviceSettings: GetSettings {
    private var defaults: UserDefaults {
        get {
            UserDefaults.standard
        }
    }
    
    public init() {}
    
    public func get() -> SettingsModel {
        let defaultCamera = (defaults.value(forKey: K.DefaultCamera) ?? K.DefaultSettings.DefaultCamera) as! Int
        let cropSize = (defaults.value(forKey: K.CropSize) ?? K.DefaultSettings.CropSize) as! Int
        let recognizeMargin = (defaults.value(forKey: K.RecognizeMargin) ?? K.DefaultSettings.RecognizeMargin) as! Float
        let timeToNewRecognize = (defaults.value(forKey: K.TimeToNewRecognize) ?? K.DefaultSettings.TimeToNewRecognize) as! Int
        let showMaskHelp = (defaults.value(forKey: K.ShowMaskHelp) ?? K.DefaultSettings.ShowMaskHelp) as! Bool
        let secretApiUrl = (defaults.value(forKey: K.SecretApiUrl) ?? K.DefaultSettings.API_BASE_URL) as! String
        let secretAuditUrl = (defaults.value(forKey: K.SecretAuditUrl) ?? K.DefaultSettings.API_AUDIT_ULR) as! String
        
        return SettingsModel(defaultCamera: defaultCamera, cropSize: cropSize, recognizeMargin: recognizeMargin, timeToNewRecognize: timeToNewRecognize, showMaskHelp: showMaskHelp, secretApiUrl: secretApiUrl, secretAuditUrl: secretAuditUrl)
    }
}
