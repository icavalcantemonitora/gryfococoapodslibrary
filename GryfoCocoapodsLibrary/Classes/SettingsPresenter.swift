import Foundation

public class SettingsPresenter {
    var getSettingsProtocol: GetSettings
    var setSettingsProtocol: SetSettings
    
    public init (getSettingsInterface: GetSettings, setSettingsInterface: SetSettings) {
        self.getSettingsProtocol = getSettingsInterface
        self.setSettingsProtocol = setSettingsInterface
    }
    
    public func getSettings() -> SettingsModel {
        return getSettingsProtocol.get()
    }
    
    public func setSettings(with settings: SettingsModel) {
        return setSettingsProtocol.set(with: settings)
    }
}
