import Foundation

public class SettingsServices {
    private var settingsPresenter: SettingsPresenter
    
    public init() {
        settingsPresenter = makeSettingsPresenter()
    }
    
    public func getSettings() -> Data {
        let settings = settingsPresenter.getSettings()
        
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(settings)
            return data
        } catch {
            print("Unable to Encode settings (\(error))")
        }
        return Data()
    }
    
    public func setSettings(with settings: Data) throws {
        let decoder = JSONDecoder()
        do {
            let settingsModel = try decoder.decode(SettingsModel.self, from: settings)
            settingsPresenter.setSettings(with: settingsModel)
        } catch {
            print("Unable to Decode settings (\(error))")
            throw error
        }
    }
}

//MARK: - SettingsPresenter factory
private func makeSettingsPresenter() -> SettingsPresenter {
    let getSettings = GetDeviceSettings()
    let setSettings = SetDeviceSettings()
    return SettingsPresenter(getSettingsInterface: getSettings, setSettingsInterface: setSettings)
}
