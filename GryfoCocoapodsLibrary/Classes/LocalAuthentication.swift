import Foundation

public class LocalAuthentication: Authentication {
    
    public init() {}
    
    private var defaults: UserDefaults {
        get {
            UserDefaults.standard
        }
    }
    
    public func authenticate<T>() -> T where T : AuthenticationModel {
        let localToken = defaults.string(forKey: K.IsAuthenticated)
        return LocalAuthenticationModel(token: localToken) as! T
    }
}
