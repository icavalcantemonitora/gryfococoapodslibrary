import Foundation

public struct K {
    public static let API_PRIVATE_ROUTE_KEY = "2EBDFB9AEEFF8AE9F18437D8D8216"
    public static let TEMPORARY_REGISTER_MINUTES_LIMIT = 1440
    
    public static let DefaultCamera = "defaultCamera"
    public static let CropSize = "cropSize"
    public static let RecognizeMargin = "recognizeMargin"
    public static let TimeToNewRecognize = "timeToNewRecognize"
    public static let ShowMaskHelp = "showMaskHelp"
    public static let SecretApiUrl = "secretApiUrl"
    public static let SecretAuditUrl = "secretAuditUrl"
    
    public struct DefaultSettings {
        public static let API_BASE_URL = "https://homolog.embedder.gryfo.com.br:5000"
        public static let API_AUDIT_ULR = "https://homolog.audit.gryfo.com.br:5000"
        
        public static let DefaultCamera = 0
        public static let CropSize = 480
        public static let RecognizeMargin = Float(0.02)
        public static let TimeToNewRecognize = 5
        public static let ShowMaskHelp = true
    }
    
    public static let IsAuthenticated = "isAuthenticated"
}
