import Foundation

public protocol SetSettings {
    func set(with settings: SettingsModel)
}
