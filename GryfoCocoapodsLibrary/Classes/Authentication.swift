import Foundation

public protocol Authentication {
    func authenticate<T: AuthenticationModel>() -> T
}
