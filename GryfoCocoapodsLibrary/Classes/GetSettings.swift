import Foundation

public protocol GetSettings {
    func get() -> SettingsModel
}
