import Foundation

public class AuthenticationServices {
    private var authenticationPresenter: AuthenticationPresenter
    
    public init() {
        authenticationPresenter = makeAuthenticationPresenter()
    }
    
    public func checkIsAuthenticated() -> Data {
        let localAuthentication = authenticationPresenter.checkIsAuthenticated()
        
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(localAuthentication)
            return data
        } catch {
            print("Unable to Encode Local Authentication Model (\(error))")
        }
        return Data()
    }
}

//MARK: - SettingsPresenter factory
private func makeAuthenticationPresenter() -> AuthenticationPresenter {
    let localAuthentication = LocalAuthentication()
    return AuthenticationPresenter(localAuthentication: localAuthentication)
}
