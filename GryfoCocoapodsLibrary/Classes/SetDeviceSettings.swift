import Foundation

public class SetDeviceSettings: SetSettings {
    private var defaults: UserDefaults {
        get {
            UserDefaults.standard
        }
    }
    
    public init() {}
    
    public func set(with settings: SettingsModel) {
        if let defaultCamera = settings.defaultCamera {
            defaults.set(defaultCamera, forKey: K.DefaultCamera)
        }
        if let cropSize = settings.cropSize {
            defaults.set(cropSize, forKey: K.CropSize)
        }
        if let recognizeMargin = settings.recognizeMargin {
            defaults.set(recognizeMargin, forKey: K.RecognizeMargin)
        }
        if let timeToNewRecognize = settings.timeToNewRecognize {
            defaults.set(timeToNewRecognize, forKey: K.TimeToNewRecognize)
        }
        if let showMaskHelp = settings.showMaskHelp {
            defaults.set(showMaskHelp, forKey: K.ShowMaskHelp)
        }
        if let secretApiUrl = settings.secretApiUrl {
            defaults.set(secretApiUrl, forKey: K.SecretApiUrl)
        }
        if let secretAuditUrl = settings.secretAuditUrl {
            defaults.set(secretAuditUrl, forKey: K.SecretAuditUrl)
        }
    }
}
