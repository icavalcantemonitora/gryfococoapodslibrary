public struct SettingsModel: Model {
    public var defaultCamera: Int?
    public var cropSize: Int?
    public var recognizeMargin: Float?
    public var timeToNewRecognize: Int?
    public var showMaskHelp: Bool?
    public var secretApiUrl: String?
    public var secretAuditUrl: String?
    
    public init(defaultCamera: Int, cropSize: Int, recognizeMargin: Float, timeToNewRecognize: Int, showMaskHelp: Bool, secretApiUrl: String, secretAuditUrl: String) {
        self.defaultCamera = defaultCamera
        self.cropSize = cropSize
        self.recognizeMargin = recognizeMargin
        self.timeToNewRecognize = timeToNewRecognize
        self.showMaskHelp = showMaskHelp
        self.secretApiUrl = secretApiUrl
        self.secretAuditUrl = secretAuditUrl
    }
    
    public static func == (lhs: SettingsModel, rhs: SettingsModel) -> Bool {
        return lhs.defaultCamera == rhs.defaultCamera &&
            lhs.cropSize == rhs.cropSize &&
            lhs.recognizeMargin == rhs.recognizeMargin &&
            lhs.timeToNewRecognize == rhs.timeToNewRecognize &&
            lhs.showMaskHelp == rhs.showMaskHelp &&
            lhs.secretApiUrl == rhs.secretApiUrl &&
            lhs.secretAuditUrl == rhs.secretAuditUrl
    }
}
