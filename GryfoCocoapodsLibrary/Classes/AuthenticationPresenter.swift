import Foundation

public class AuthenticationPresenter {
    var localAuthentication: LocalAuthentication
    
    public init (localAuthentication: LocalAuthentication) {
        self.localAuthentication = localAuthentication
    }
    
    public func checkIsAuthenticated() -> LocalAuthenticationModel {
        return localAuthentication.authenticate()
    }
}
