//
//  ViewController.swift
//  GryfoCocoapodsLibrary
//
//  Created by IsaacCavalcante on 07/27/2021.
//  Copyright (c) 2021 IsaacCavalcante. All rights reserved.
//

import UIKit
import GryfoCocoapodsLibrary

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settingsService = SettingsServices()
        var settings = settingsService.getSettings()
        print(settings)
        
        do {
            var dictionary = try? JSONSerialization.jsonObject(with: settings, options: .allowFragments) as? [String: Any]
            
            print(dictionary)
            
            dictionary??["cropSize"] = 240
            
            settings = try JSONSerialization.data(withJSONObject: dictionary ?? [:], options: .prettyPrinted)
            
        } catch {
            print(error)
        }
        
        do {
            try settingsService.setSettings(with: settings)
        } catch {
            print(error)
        }
        
        settings = settingsService.getSettings()
        print(settings)
        
        do {
            let dictionary = try? JSONSerialization.jsonObject(with: settings, options: .allowFragments) as? [String: Any]
            
            print(dictionary)
        } catch {
            print(error)
        }
        
        let x = AuthenticationServices()
        let data = x.checkIsAuthenticated()
        
        let dictionary = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
        
        print(dictionary)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

