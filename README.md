# GryfoCocoapodsLibrary

[![CI Status](https://img.shields.io/travis/IsaacCavalcante/GryfoCocoapodsLibrary.svg?style=flat)](https://travis-ci.org/IsaacCavalcante/GryfoCocoapodsLibrary)
[![Version](https://img.shields.io/cocoapods/v/GryfoCocoapodsLibrary.svg?style=flat)](https://cocoapods.org/pods/GryfoCocoapodsLibrary)
[![License](https://img.shields.io/cocoapods/l/GryfoCocoapodsLibrary.svg?style=flat)](https://cocoapods.org/pods/GryfoCocoapodsLibrary)
[![Platform](https://img.shields.io/cocoapods/p/GryfoCocoapodsLibrary.svg?style=flat)](https://cocoapods.org/pods/GryfoCocoapodsLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GryfoCocoapodsLibrary is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GryfoCocoapodsLibrary'
```

## Author

IsaacCavalcante, isaac.cavalcante@monitoratec.com.br

## License

GryfoCocoapodsLibrary is available under the MIT license. See the LICENSE file for more info.
