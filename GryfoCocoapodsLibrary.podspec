#
# Be sure to run `pod lib lint GryfoCocoapodsLibrary.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GryfoCocoapodsLibrary'
  s.version          = '0.1.5'
  s.summary          = 'This Pod is a test for Gryfo'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This pod is a test to use with Gryfo iOS wrapper
                       DESC

  s.homepage         = 'https://gitlab.com/icavalcantemonitora/gryfococoapodslibrary'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'IsaacCavalcante' => 'isaac.cavalcante@monitoratec.com.br' }
  s.source           = { :git => 'https://gitlab.com/icavalcantemonitora/gryfococoapodslibrary.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.swift_version = '5.4'
  s.ios.deployment_target = '10.0'

  s.source_files = 'GryfoCocoapodsLibrary/Classes/**/*'
  
  # s.resource_bundles = {
  #   'GryfoCocoapodsLibrary' => ['GryfoCocoapodsLibrary/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
